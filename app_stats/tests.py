from django.test import TestCase
from django.urls import resolve
from django.test import Client
from datetime import datetime
from .views import index
import unittest

# Create your tests here.

class HomepageUnitTest(TestCase):
    
    def test_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)

    def test_hello_name_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

from django.shortcuts import render
from app_profile.models import pro
from app_status.models import Message
from app_add_friend.models import Add_Friend

# Create your views here.
response = {}
def index(request):
    try:
        latest_status = Message.objects.latest('created_date')
    except Message.DoesNotExist:
        latest_status = None
        
    response['name'] = pro.name
    response['latest'] = latest_status
    response['number_of_posts'] = Message.objects.all().count()
    response['number_of_friends'] = Add_Friend.objects.all().count()
    return render(request, 'app_stats/stats.html', response)

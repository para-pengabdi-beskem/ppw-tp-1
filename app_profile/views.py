from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Profile
from .models import pro

# from ..app_profile.models import Profile

response = {}
# Create your views here.
def index(request): # Temporary, to allow testcase to succeed
	response['name'] = pro.name
	response['birthday'] = pro.birthday
	response['gender'] = pro.gender
	response['expertise'] = pro.expertise
	response['description'] = pro.description
	response['email'] = pro.email
	return render(request, 'profile.html', response)
	
def delete(request):
	Profile.objects.all().delete()
	print(Profile.objects.all())
	return render(request, 'profile.html')
	
def profile(request, profile_id=pro.id):
	prof = Profile.objects.get(id=profile_id)

	dictio = {
		'id' : id,
		'name' : prof.name,
		'birthday' : prof.birthday,
		'gender' : prof.gender,
		'expertise' : prof.expertise,
		'description' : prof.description,
		'email' : prof.email,
	}
	return render(request, 'profile.html', dictio)
from django.test import TestCase
from django.urls import resolve
from django.test import Client
from django.http import HttpRequest
from datetime import datetime
from .views import index, profile, delete
from .models import Profile
import unittest

# Create your tests here.
class ProfileTestTest(TestCase):

	def test_profile_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, index)
	
	def test_print_model(self):
		Profile.objects.create(id='5678',name='Naufal is ok I guess',birthday=datetime(1100,5,7),gender='Male',expertise='Being fat',description='lorem ipsum',email='Naufal@nyahoo.cum')
		response = Client().get('/profile/5678')
		self.assertEqual(response.status_code,200)
		
	def test_delete_model(self):
		Profile.objects.create(id='5678',name='Naufal is ok I guess',birthday=datetime(1100,5,7),gender='Male',expertise='Being fat',description='lorem ipsum',email='Naufal@nyahoo.cum')
		request = HttpRequest()
		delete(request)
		self.assertEqual(Profile.objects.count(),0)
	
	def test_self_assert_in(self):
		Profile.objects.create(id='5678',name='Naufal is ok I guess',birthday=datetime(1100,5,7),gender='Male',expertise='Being fat',description='lorem ipsum',email='Naufal@nyahoo.cum')
		request = HttpRequest()
		response = profile(request,'5678')
		self.assertIn('Naufal is ok I guess', response.content.decode('utf-8'))
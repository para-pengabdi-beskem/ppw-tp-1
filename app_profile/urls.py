from django.conf.urls import url
from .views import index, profile, delete
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^$', index, name='index'),
	#url(r'^$', RedirectView.as_view(url='/profile/1234/', permanent=False), name='index'),
	url(r'^delete$', delete, name='delete'),
	url(r'^(\w+)?$', profile, name='profile')
]

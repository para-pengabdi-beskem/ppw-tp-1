from django.db import models
from datetime import datetime

# Create your models here.

class Profile(models.Model):
	id = models.CharField(max_length=10, primary_key=True)
	
	name = models.CharField(max_length=25)
	birthday = models.DateField()
	gender = models.CharField(max_length=6)
	expertise = models.CharField(max_length=100)
	description = models.CharField(max_length=100)
	email = models.EmailField(max_length=50)
	
	def __str__(self):
		return '('+self.id+':'+self.name+')'

pro = Profile(id='5678',name='Naufal is ok I guess',birthday=datetime(1100,5,7),gender='Male',expertise='Being fat',description='lorem ipsum',email='Naufal@nyahoo.cum')
pro.save()
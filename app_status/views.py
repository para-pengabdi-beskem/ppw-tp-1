from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Create your views here.

response = {'author': "Para Pengabdi Beskem"}

def index(request):
    html = 'app_status/status.html'
    response['message_form'] = Message_Form
    message = Message.objects.all()
    response['message'] = message
    return render(request, html, response)

def update_status(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        message = Message(message=response['message'])
        message.save()
        html = 'app_status/status.html'
        return HttpResponseRedirect('/status/')
    else:        
        return HttpResponseRedirect('/status/')

def show_post(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'app_status/status.html'
    return render(request, html , response)

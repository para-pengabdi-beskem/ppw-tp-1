from django.conf.urls import url
from .views import index, update_status, show_post

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^update_status', update_status, name='update_status'),
    url(r'^show_post', show_post, name='show_post'),
]

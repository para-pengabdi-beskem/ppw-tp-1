from django.test import TestCase, Client
from django.urls import resolve
from .models import Message
from .forms import Message_Form
from .views import index, update_status, show_post

# Create your tests here.

class AppStatusTest(TestCase):
    def test_app_status_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)
    
    def test_app_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

#############################################################

    def test_model_can_create_new_message(self):
        #Creating a new activity
        new_activity = Message.objects.create(message='This is a test')

        #Retrieving all available activity
        counting_all_available_message= Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_form_validation_for_blank_items(self):
        form = Message_Form(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )
        
#############################################################

    def test_app_status_post_fail(self):
        response = Client().post('/status/update_status', {'message': ''})
        self.assertEqual(response.status_code, 302)

    def test_app_status_post_success_and_render_the_result(self):
        message = 'Test'
        response = Client().post('/status/update_status', {'message': message})
        self.assertEqual(response.status_code, 302)     # CHECK

#############################################################

    def test_app_status_table_using_message_table_func(self):
        found = resolve('/status/show_post')
        self.assertEqual(found.func, show_post)

    def test_app_status_showing_all_messages(self):

        test_message = 'Test Post'
        test_data = {'message': test_message}
        test_data_post = Client().post('/status/update_status', test_data)
        self.assertEqual(test_data_post.status_code, 302)   # CHECK

        response = Client().get('/status/show_post')
        html_response = response.content.decode('utf8')

        for key,data in test_data.items():
            self.assertIn(data,html_response)


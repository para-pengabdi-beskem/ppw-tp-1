from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Please fill the input here',
    }
    attrs = {
        'class': 'form-control',
        'cols': 8,
        'rows': 4,
    }

    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)

from django.apps import AppConfig


class AppAddFriendConfig(AppConfig):
    name = 'app_add_friend'

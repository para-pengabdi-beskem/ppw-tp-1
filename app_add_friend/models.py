from django.db import models

# Create your models here.
class Add_Friend(models.Model):
    name = models.CharField(max_length=25)
    url = models.URLField(default='',max_length=200)
    created_date = models.DateTimeField(auto_now_add=True)

from django import forms

# Create your forms here.
class Add_Friend_Form(forms.Form):
    error_message = {
        'required': 'Please fill out this field'
        }
    attrs = {
        'class': 'form-control'
        }
           
    name = forms.CharField(label='Name', required=True, max_length=25, widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=attrs))

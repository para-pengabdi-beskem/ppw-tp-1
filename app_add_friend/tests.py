from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, Add_New_Friend
from .models import Add_Friend
from .forms import Add_Friend_Form

# Create your tests here.
class AppAddFriendTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friend(self):
        # Creating a new friend
        new_friend = Add_Friend.objects.create(name='Muhammad Wildan Aziz', url='https://gitlab.com/wildanazz')

        # Retrieving all available friends
        counting_all_friend = Add_Friend.objects.all().count()
        self.assertEqual(counting_all_friend, 1)
        
    def test_form_validation_for_blank_items(self):
        form = Add_Friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.error_message['required'], "Please fill out this field")
    
    def test_add_friend_post_success_and_render_the_result(self):
        testname = 'Muhammad Wildan Aziz'
        testurl = 'https://gitlab.com/wildanazz'
        response_post = Client().post('/add-friend/Add_New_Friend', {'name': testname, 'url': testurl})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(testname, html_response)
        self.assertIn(testurl, html_response)

    def test_add_friend_post_error_and_render_the_result(self):
        testname = 'Muhammad Wildan Aziz'
        testurl = 'https://gitlab.com/wildanazz'
        response_post = Client().post('/add-friend/Add_New_Friend', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(testname, html_response)
        self.assertNotIn(testurl, html_response)

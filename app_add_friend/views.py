from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Friend_Form
from .models import Add_Friend

# Create your views here.
response = {}
def index(request):
    response['author'] = "Muhammad Wildan Aziz"
    add_friend = Add_Friend.objects.all()
    response['add_friend'] = add_friend
    html = 'add_friend/add_friend.html'
    response['add_friend_form'] = Add_Friend_Form
    return render(request, html, response)

def Add_New_Friend (request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        add_friend = Add_Friend(name=response['name'],url=response['url'])
        add_friend.save()
        return HttpResponseRedirect('/add-friend/')
    else:
        return HttpResponseRedirect('/add-friend/')

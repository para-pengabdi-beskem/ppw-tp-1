from django.conf.urls import url
from .views import index, Add_New_Friend

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^Add_New_Friend', Add_New_Friend, name='Add_New_Friend'),
]

**Members:**
- Rayhan Naufal Hendry --> Status
- Muhammad Wildan Aziz --> Add Friend
- Alfathar Yusvi H --> Profile
- Yuris Aryansyah S.C --> Navbar, Stats, Header, Footer

**Pipeline Status:** [![pipeline status](https://gitlab.com/para-pengabdi-beskem/ppw-tp-1/badges/master/pipeline.svg)](https://gitlab.com/para-pengabdi-beskem/ppw-tp-1/commits/master)

**Code Coverage:** [![coverage report](https://gitlab.com/para-pengabdi-beskem/ppw-tp-1/badges/master/coverage.svg)](https://gitlab.com/para-pengabdi-beskem/ppw-tp-1/commits/master)

**Herokuapp:** https://pandapacil.herokuapp.com/
"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import app_stats.urls as home
import app_add_friend.urls as app_add_friend
import app_status.urls as app_status
import app_profile.urls as app_profile
from app_stats.views import index as index_stats

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^stats/', include(home, namespace='home')),
    url(r'^add-friend/', include(app_add_friend, namespace='app-add-friend')),
    url(r'^status/', include(app_status, namespace='app-status')),
    url(r'^profile/', include(app_profile, namespace='profile')),
    url(r'^$', index_stats, name='index'),
]
